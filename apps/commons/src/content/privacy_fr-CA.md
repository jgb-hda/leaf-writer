# Conditions d'utilisation et politique de confidentialité

Cette politique régit votre utilisation de LEAF-Writer ; en utilisant LEAF-Writer, vous acceptez cette politique dans son intégralité. Si vous n'êtes pas d'accord avec cette politique ou toute partie de cette politique, vous ne devez pas utiliser ce site Web.

LEAF-Writer utilise des cookies. En utilisant LEAF-Writer et en acceptant cette politique, vous consentez à notre utilisation des cookies conformément à la politique relative aux cookies [link].

Les documents sur LEAF-Writer sont fournis « tels quels ». LEAF-Writer ne donne aucune garantie, expresse ou implicite, et décline et annule par la présente toutes les autres garanties, y compris, sans s'y limiter, les garanties ou conditions implicites de qualité marchande, d'adéquation à un usage particulier ou de non-violation de la propriété intellectuelle ou autre violation des droits.

De plus, LEAF-Writer ne garantit ni ne fait aucune déclaration concernant l'exactitude, les résultats probables ou la fiabilité de l'utilisation du matériel sur son site Web ou autrement lié à ce matériel ou sur tout site lié à ce site.

## Liens externes

LEAF-Writer n'examine pas régulièrement tous les sites qui y sont liés ou qui y sont liés et n'est pas responsable du contenu de ces sites liés. L'inclusion de tout lien n'implique pas l'approbation par LEAF-Writer du site. L'utilisation d'un tel site Web lié est aux risques et périls de l'utilisateur. Il vous est conseillé de consulter la politique de confidentialité de chaque site que vous visitez, car cette politique de confidentialité ne s'applique pas à ce contenu tiers. LEAF-Writer n'a aucun contrôle sur et n'assume aucune responsabilité pour le contenu, les politiques de confidentialité ou les pratiques de tout site ou service tiers.

## Exigences légales

Le projet LINCS est basé au Canada. Les termes de cette politique ont été préparés conformément aux exigences légales canadiennes.

## Collecte et stockage des données

### Quelles données sont collectées

### Comment les données sont collectées

### Comment les données sont utilisées

### Comment les données sont stockées

### Combien de temps les données sont stockées

Les données personnelles que nous traitons à quelque fin que ce soit ne seront pas conservées plus longtemps que [****] nécessaire aux fins pour lesquelles elles sont collectées.

## Biscuits

La plupart des navigateurs Web sont configurés pour accepter les cookies par défaut. Si vous préférez, vous pouvez choisir de paramétrer votre navigateur pour supprimer les cookies et/ou rejeter les cookies. Si vous choisissez de supprimer les cookies ou de rejeter les cookies, cela pourrait affecter certaines fonctionnalités ou certains services.

### Quels cookies nous installons

#### Nécessaire

#### Préférences

#### Statistiques

#### Commercialisation

Aucun.

#### Non classé (divers)

Aucun.

### Comment nous utilisons les cookies

## Conditions supplémentaires

## Modifications de cette politique

Cette politique peut être mise à jour à tout moment. Les mises à jour seront publiées sur cette page. Nous vous encourageons à consulter fréquemment cet avis de confidentialité pour être informé de la manière dont nous protégeons vos informations.

## Contactez-nous

Si vous avez des questions ou des commentaires sur cette politique, contactez-nous à [cwrc-leaf@ualberta.ca](mailto:cwrc-leaf@ualberta.ca).

## Remerciements

Lors de la préparation de cette politique, LEAF-Writer s'est appuyé sur des documents de politique similaires de KgBase, Metaphacts et Mobi.
