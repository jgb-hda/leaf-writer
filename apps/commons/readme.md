# LEAF-Writer Commons

![GPL-2.0](https://img.shields.io/badge/license-GLP--2.0-orange)
![type definitions](https://img.shields.io/badge/types-TypeScript-blue)

## To do

- Setup test unit
- Documentation
  - Writer readme file
