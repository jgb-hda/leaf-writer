import { Language } from './Language';
import { ThemeAppearance } from './ThemeAppearance';

export const Interface = () => (
  <>
    <ThemeAppearance />
    <Language />
  </>
);
